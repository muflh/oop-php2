<?php

require_once ("animal.php");
require_once ("frog.php");
require ("ape.php");

$object = new Animal("shaun");

echo "Animal name :   $object->name  <br>"; // "shaun"
echo "Legs :  $object->legs <br>"; // 2
echo "cold blood :  $object->cold_blooded  <br><br>"; // false

$object2 = new Frog ("Buduk");

echo "Animal name :   $object2->name  <br>"; 
echo "Legs :  $object2->legs <br>"; 
echo "cold blood :  $object2->cold_blooded  <br>"; 
$object2->jump()  ;
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Animal name :   $sungokong->name  <br>"; 
echo "Legs :  $sungokong->legs <br>"; 
echo "cold blood :  $sungokong->cold_blooded  <br>"; 
$sungokong->yell();
?>
